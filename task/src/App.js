import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [inputValue, setInputValue] = useState({
    value: '',
    containsA: false,
  });
  const [buttonsState, setButtonsState] = useState({
    button1Disabled: false,
    button2Disabled: false,
  })
  useEffect(() => {
      console.log('Component loaded correctly"');
  }, []);
 
  const onInputChange = (value) => setInputValue({
      value,
      containsA: value?.includes('a'),
  });
  
  return (
    <div className="container">
      <div className='buttonsGroup'>
        <button 
          disabled={buttonsState.button1Disabled}
          onClick={() => setButtonsState({
            button1Disabled: true,
            button2Disabled: false,
          })}
        >
          Active
        </button>
        <div className="seperator" />
        <button 
          disabled={buttonsState.button2Disabled}
          onClick={() => setButtonsState({
            button1Disabled: false,
            button2Disabled: true,
          })}
        >
          Inactive
        </button>
      </div>
      <div className="card">
        <div className="logoContainer"><img src={logo} className="logo" alt="logo" /></div>
        <div className='elementContainer'>
          <div className='inputContainer'>
            <input 
              className={inputValue.containsA && 'backgroundGreen'}
              onChange={e => onInputChange(e.target.value)}
            />
          </div>
          <div className='saveButton'>
            <button onClick={() => console.log(inputValue.value)}>SAVE</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
